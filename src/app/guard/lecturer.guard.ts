import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationServiceService } from '../service/authentication-service.service';


@Injectable({
  providedIn: 'root'
})
export class LecturerGuard implements CanActivate {
  constructor(private router: Router,private authService:AuthenticationServiceService){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):Observable<boolean> | Promise<boolean> | boolean {
      if (this.authService.hasRole('LECTURER')){
        return true;
      }
    this.router.navigate(['/login'],{queryParams: {returnUrl: state.url}
  });
  return false;
  }
}
